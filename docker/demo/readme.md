## References 

https://docs.docker.com/engine/reference/commandline/cli/



## Installation

https://docs.docker.com/engine/install/


## Running Nginx on docker container

### Run it


```bash
  docker run --name ournginx -p 80:80 -d nginx
```

--name : container name

-p : map port 80 on our machine to the container port 80 

-d : run in background

nginx : image name

### Get logs
```bash
docker logs ournginx
```

## Running Postgres Server on docker container

### Run it

```bash
docker run --name ourpostgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

-e : to add env variables

### Exec to  the container

```bash
docker exec -it  ourpostgres bash
```
-it: interactive 

bash : bash cli

### Enter to the server from the container cli

```bash
psql -U postgres
```



