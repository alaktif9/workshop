
# Backend 

Flask CRUD API
This is a basic CRUD (`Create`, `Read`, `Update`, `Delete`) API built using Flask and SQLAlchemy to manage Aliens.

## Setup
1. *Clone this repository.*

2. *Install the necessary dependencies:*

    ```bash
    pip install -r requirements.txt
    ```

3. *Configure your PostgreSQL database and set the essential environment variables (`DB_USERNAME`, `DB_PASSWORD`, `DB_NAME`, `DB_HOST`, `DB_PORT`) or utilize the default variables.*
## Running the API
*Run the Flask application by executing*
```bash
  python app.py
```
*By default, the API will be available at http://localhost:9090.*


## Endpoints
- *GET /aliens*

Retrieve a list of all aliens.

- *POST /aliens*

Add a new aliens by providing name and description in the request body.

- *PUT /aliens/<aliens_id>*

Update an existing aliens by providing the aliens_id, name, and description in the request body.

- *DELETE /aliens/<aliens_id>*

Delete an existing aliens by providing the aliens_id.

## Dockerize the backend

move to the root folder `workshop/docker/backend`

- *build* 

```bash
docker build -t spaceland-backend   .
```
- *tag the image* 

```bash
docker tag spaceland-backend  `registry-name`/spaceland-backend:0.0.1
```
*replace - registry-name - with yours*

- *push to hub* 
```bash
docker push `registry-name`/spaceland-backend:0.0.1
```


# FrontEnd

A basic CRUD Angular project that trigers /api


- *package.json*

```bash
    "start:project": "ng serve --proxy-config proxy.dev.conf.json",
```
- *proxy.dev.conf.json*
```bash
{
  "/api": {
    "target": "http://localhost:9090",
    "secure": false,
    "pathRewrite": {
      "^/api": ""
    }
  }
}
```

## Run the App

- *install the dependencies*

```bash
npm ci & npm install
```

- *start the server*

```bash
npm run start:project
```

*By default, the App will be hosted at http://localhost:4200*

## Dockerize the frontend

move to the root folder `workshop/docker/frontend`

- *build* 

```bash
docker build -t spaceland-frontend   .
```
- *tag the image* 

```bash
docker tag spaceland-frontend `registry-name`/spaceland-frontend:0.0.1
```
*replace - registry-name - with yours*

- *push to hub* 

```bash
docker push `registry-name`/spaceland-frontend:0.0.1
```

# Deploy the app on docker 

## Set Up

### *Create docker network for the app*

```bash
docker network create spaceland
```

### *Create Postgres server in docker container using spaceland network*

```bash
docker run --name spaceland-db -e POSTGRES_DB=spaceland -e POSTGRES_USER=alien -e POSTGRES_PASSWORD=alienpass -d --net spaceland postgres
```

### *Create Database of the server*

```bash
docker exec -it spaceland-db bash  # Exec to the container with bash

psql -U alien -d spaceland  # conect to server with user alien and database spaceland

CREATE TABLE alien (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    description VARCHAR(255)
); # create table for the backend
```

### *Runing the backend*

```bash
docker run --m spaceland-backend -e DB_USERNAME=alien -e DB_PASSWORD=alienpass -e DB_NAME=spaceland -e DB_PORT=5432 -e DB_HOST=spaceland-db --net spaceland -p 9090:9090 -d `registry-name`/spaceland-backend:0.0.1
```

*`spaceland-db` is a dns provided by docker for the containers running on the same network, it represent the name of the container and redirect to the container ip adress*

### *Runing the frontend*

#### *configure the nginx reverse proxy*

- *nginx.conf file*

```bash
events{}
http {
    include /etc/nginx/mime.types;
    server {
        listen 80;
        server_name localhost;
        root /usr/share/nginx/html;
        index index.html;
        location / {
            try_files $uri $uri/ /index.html;
        }
        location /api/ {
            proxy_pass http://spaceland-backend:9090/;
        }
    }
}
```

- *build again & run*


```bash
docker run --m spaceland-frontend --net spaceland -p 80:80 -d `registry-name`/spaceland-frontend:0.0.1
```

* Go to http://localhost:80*



# Deploy the app on docker compose

*in the root folder we have a docker-compose.yaml file that containes 3 services*

*we can change the image of each version*

### *Runing the docker compose*

```bash
docker-compose up
```

* Go to http://localhost:80*