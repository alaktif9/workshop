apiVersion: apps/v1
kind: Deployment
metadata:
  name: spaceland-backend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: spaceland-backend
  template:
    metadata:
      labels:
        app: spaceland-backend
    spec:
      initContainers:
      - image: bitnami/postgresql:11.12.0-debian-10-r13
        name: spaceland-init-container
        env:
          - name: DB_USERNAME
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_USERNAME
          - name: DB_NAME
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_NAME
          - name: DB_HOST
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_HOST
          - name: DB_PORT
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_PORT
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: spaceland-secrete
                key: DB_PASSWORD
        command: ['sh', '-c', "/spaceland-init-script.sh"]
        volumeMounts:
            - name: init-script
              mountPath: /spaceland-init-script.sh 
              subPath: spaceland-init-script.sh

      containers:
      - name: spaceland-backend
        image: alaktif9/sl_backend:0.0.2
        env:
          - name: DB_USERNAME
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_USERNAME
          - name: DB_NAME
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_NAME
          - name: DB_HOST
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_HOST
          - name: DB_PORT
            valueFrom:
              configMapKeyRef:
                name: spaceland-config
                key: DB_PORT
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: spaceland-secrete
                key: DB_PASSWORD
        ports:
        - containerPort: 9090

      volumes:
        - name: init-script
          configMap:
            name: spaceland-init
            defaultMode: 0755


---

apiVersion: v1
kind: ConfigMap
metadata:
  name: spaceland-init
data:
  spaceland-init-script.sh : |-
    #!/bin/bash

    echo "HOST : $DB_HOST"
    echo "Creating Database: $DB_NAME"
    echo "Backend User: $DB_USERNAME"

    # Check if the database exists; if not, create it
    PGPASSWORD=$DB_PASSWORD psql -w -d spaceland -U $DB_USERNAME -h $DB_HOST -tc "SELECT 1 FROM pg_database WHERE datname = '$DB_NAME'" |
        grep -q 1 ||
        PGPASSWORD=$DB_PASSWORD psql -d spaceland -U $DB_USERNAME -h $DB_HOST -c "CREATE DATABASE $DB_NAME" || true