

# Connect to cluster




gcloud auth login 

gcloud container clusters get-credentials spaceland --region europe-southwest1 --project ivory-folder-406609

kubectl get nodes -o wide



# deploying the application

## deploying the database server

cd database

kubectl apply -f .


kubectl get pods -o wide -w

kubectl describe pod spaceland-db-0


kubectl exec -it spaceland-db-0 sh


psql -U alien -d spaceland


CREATE TABLE alien (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    description VARCHAR(255)
);

## deploying the backend


DB_PG_PASS: cG9zdGdyZXM=

cd backend

kubectl apply -f .


## deploying the front end


cd backend


kubectl apply -f .



# routing managment with ingress

##  deploying the ingress controller with helm

helm install my-nginx-ingress-controller bitnami/nginx-ingress-controller --version 9.9.5

## deploy the ingress without tls config

kubectl apply -f ingress_with_tls.yaml

## configure cloud-dns and the domain provider dns


# adding certificate

## deploy the cert manager

with helm : 

helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.5.4 \
  --set global.leaderElection.namespace=cert-manager --set 'extraArgs={--dns01-recursive-nameservers-only,--dns01-recursive-nameservers=8.8.8.8:53\,1.1.1.1:53}'

with yaml file :

cd addons 
cd tls
kubectl apply -f cert-manager.yaml




## deploy the issuer 

cd addons 
cd tls
kubectl apply -f ClusterIssuer.yaml

## deploy the certificate
