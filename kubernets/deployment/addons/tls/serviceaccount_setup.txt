export PROJECT_ID=ivory-folder-406609

gcloud iam service-accounts create dns01-solver2 --display-name "dns01-solver2"

gcloud projects add-iam-policy-binding $PROJECT_ID \
   --member serviceAccount:dns01-solver2@$PROJECT_ID.iam.gserviceaccount.com \
   --role roles/dns.admin

gcloud iam service-accounts keys create service_account.json \
   --iam-account dns01-solver2@$PROJECT_ID.iam.gserviceaccount.com

kubectl create secret generic clouddns-dns01-solver2-svc-acct \
   --from-file=service_account.json

kubectl annotate serviceaccount --namespace=cert-manager cert-manager \
    "iam.gke.io/gcp-service-account=dns01-solver2@$PROJECT_ID.iam.gserviceaccount.com"